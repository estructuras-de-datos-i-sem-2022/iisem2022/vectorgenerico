/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Cadena;

/**
 *
 * @author madarme
 */
public class TestGenerico {

    public static void main(String[] args) {

        //Por defecto usaré Wrapper
        //int --> Integer, float -> Float,char--> Character
        Integer a = 3, b = 5;
        /*if (a >= b) {
            System.out.println(":)");
        } else {
            System.out.println(":(");
        }*/

        Float x = 4.5F, y = 6.7F;
        /*if (x >= y) {
            System.out.println(":)");
        } else {
            System.out.println(":(");
        }*/

        comparar(a, b);
        comparar(x, y);

        Cadena c1 = new Cadena("Maria");
        Cadena c2 = new Cadena("maria");

        comparar(c1, c2);

        Integer r[] = {4, 1, 2, 7, 9, 0};
        Cadena cadenas[]={new Cadena("Maria"),new Cadena("Ana"),new Cadena("Xiomy")};
        imprimeVector(r);
        imprimeVector(cadenas);
        
        System.out.println("Ordenando:");
        
        burbuja(r);
        burbuja(cadenas);
        
        imprimeVector(r);
        imprimeVector(cadenas);
    }

    /**
     * REQUISITO:
     *  1.La clase del objeto debe tener implementado toString.
     * @param <T>
     * @param v 
     */
    public static <T> void imprimeVector(T[] v) {
        String msg = "";
        for (T elemento : v) {
            msg += elemento.toString() + "\n";
        }
        System.out.println(msg);

    }

    /**
     * Requisitos: 1. Clase del objeto debe tener compareTo 2. Clase del objeto
     * debe tener toString
     *
     *
     *
     * @param <T> Un tipo de clase objeto
     * @param obj1
     * @param obj2
     */
    public static <T> void comparar(T obj1, T obj2) {

        int c = ((Comparable) (obj1)).compareTo(obj2);
        if (c == 0) {
            System.out.println(obj1.toString() + " == " + obj2.toString());
        } else if (c > 0) {
            System.out.println(obj1.toString() + " > " + obj2.toString());
        } else {
            System.out.println(obj1.toString() + " < " + obj2.toString());
        }

    }

    private static <T> void burbuja(T[] arreglo) {
        for (int x = 0; x < arreglo.length; x++) {
            // Aquí "y" se detiene antes de llegar
            // a length - 1 porque dentro del for, accedemos
            // al siguiente elemento con el índice actual + 1
            for (int y = 0; y < arreglo.length - 1; y++) {
                T elementoActual = arreglo[y],
                        elementoSiguiente = arreglo[y + 1];

                int c = ((Comparable) elementoActual).compareTo(elementoSiguiente);
                if (c > 0) {
                    // Intercambiar
                    arreglo[y] = elementoSiguiente;
                    arreglo[y + 1] = elementoActual;
                }
            }
        }
    }

}
